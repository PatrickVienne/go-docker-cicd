#!/bin/sh
# wait-for-postgres.sh

set -e
  
host="$1"
shift
  
until PGPASSWORD=$POSTGRES_PASSWORD psql -h "$host" -U "postgres" -c '\q'; do
  >&2 echo "Postgres is unavailable - sleeping, " + `{$POSTGRES_PASSWORD} + {$host}`
  sleep 1
done
  
>&2 echo "Postgres is up - executing command"
exec "$@"
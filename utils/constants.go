package utils

import (
	"fmt"
	"os"
)

var VERSION = ""

func init() {
	f, err := os.ReadFile("VERSION")
	if err != nil {
		panic(fmt.Errorf("could not find VERSION file:", err))
	}
	VERSION = string(f)
}

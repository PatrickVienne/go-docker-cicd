package calc

import "testing"

func TestAdder(t *testing.T) {
	if Adder(1, 2, 3) != 6 {
		t.Fatalf("Adder(1,2,3): %d != %d", Adder(1, 2, 3), 6)
	}

	if Adder(-1, 1) != 0 {
		t.Fatalf("Adder(-1,1): %d != %d", Adder(-1, 1), 0)
	}
}

func BenchmarkAdder(b *testing.B) {
	a := make([]int, 1000)
	for k := 0; k < 1000; k++ {
		a[k] = k
	}
	for i := 0; i < b.N; i++ {
		Adder(a...)
	}
}

#!/bin/sh -x
VERSION=$(cat VERSION)
echo "pushing version $VERSION"
docker login -u$DOCKER_ID_USER -p$DOCKER_PW


docker build -t $DOCKER_IMG:$VERSION -t $DOCKER_IMG:latest .

docker tag $DOCKER_IMG $DOCKER_ID_USER/$DOCKER_IMG:$VERSION
docker push $DOCKER_ID_USER/$DOCKER_IMG:$VERSION

docker tag $DOCKER_IMG $DOCKER_ID_USER/$DOCKER_IMG:latest
docker push $DOCKER_ID_USER/$DOCKER_IMG:latest